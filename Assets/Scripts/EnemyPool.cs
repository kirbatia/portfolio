using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    [SerializeField] private GameObject _enemyToPool;
    [SerializeField] private int _poolSize = 10; // Can be changed in Inspector
    private List<GameObject> _pooledEnemies = new();
    
    public static EnemyPool Instance;

    private void Awake()
    {
        Instance = this;
    }
    
    private void Start()
    {
        _pooledEnemies = new List<GameObject>();

        for (int i = 0; i < _poolSize; i++)
        {
            GameObject obj = Instantiate(_enemyToPool, this.transform);
            obj.SetActive(false);
            _pooledEnemies.Add(obj);
        }
    }

    public GameObject GetPooledObject(Vector3 position)
    {
        foreach (var enemy in _pooledEnemies)
        {
            if (!enemy.activeInHierarchy)
            {
                return enemy;
            }
        }

        return null;
    }
}