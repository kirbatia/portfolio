using TMPro;
using UnityEngine;

public class InfoView : MonoBehaviour
{
   [SerializeField] private TextMeshProUGUI _scoreText;
   [SerializeField] private TextMeshProUGUI _distanceText;

   private void OnEnable()
   {
      OnScoreChanged();
      OnDistanceChanged();
      DataStorage.ScoreChanged += OnScoreChanged;
      DataStorage.DistanceChanged += OnDistanceChanged;
   }

   private void OnDisable()
   {
      DataStorage.ScoreChanged -= OnScoreChanged;
      DataStorage.DistanceChanged -= OnDistanceChanged;
   }

   private void OnDistanceChanged()
   {
     _distanceText.text = $"Distance : {DataStorage.Distance:F}";
   }

   private void OnScoreChanged()
   {
      _scoreText.text = $"Score : {DataStorage.Score}";
   }
}