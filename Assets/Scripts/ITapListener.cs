﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface ITapListener
{
    public event Action<Vector3, bool> MoveStarted;
    public event Action<List<Vector3>> LineFollowed;
}