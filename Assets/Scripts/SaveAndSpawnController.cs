using System.Collections;
using UnityEngine;

public class SaveAndSpawnController : MonoBehaviour
{
    // it is better practice to split this script. I left it like this because there is also a term like "over engineering". Script is small.
    // If there were more logic or bigger project - I'd split it
    private void OnEnable()
    {
        if (PlayerPrefs.HasKey("Score"))
        {
            DataStorage.Score = PlayerPrefs.GetInt("Score");
        }
        
        if (PlayerPrefs.HasKey("Distance"))
        {
            DataStorage.Distance = PlayerPrefs.GetFloat("Distance");
        }
    }

    private void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("Score", DataStorage.Score);
        PlayerPrefs.SetFloat("Distance", DataStorage.Distance);
        PlayerPrefs.Save();
    }
    
    private IEnumerator SpawnEnemies()
    {
        var pool = EnemyPool.Instance;
        for (var i = 0; i < int.MaxValue; i++)
        {
            var time = Random.Range(0.5f, 3f);
            var position = new Vector3(Random.Range(-3f, 3f), Random.Range(-3f, 3f), 0); // here we can check the screen width / height,
                                                                                               // transform them to the WorldSpace and arrange the positions limit
            var square = pool.GetPooledObject(position);
            
            if (square != null)
            {
                square.SetActive(true);
                square.transform.position = position;
            }

            yield return new WaitForSeconds(time);
        }
    }
}