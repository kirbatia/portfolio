using System;
using System.Collections.Generic;
using UnityEngine;

public class TapListener : MonoBehaviour, ITapListener
{
    private readonly List<Vector3> _mousePositionsLine = new();
    private bool _isClicked;
    private float _timer;
    private Vector3 _deltaPosition;
    private Vector3 _playerTargetPos;
    private Camera _camera;
    private float _followMouseSaveTimer;
    
    public event Action<Vector3, bool> MoveStarted;
    public event Action<List<Vector3>> LineFollowed;

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isClicked = true;
            _mousePositionsLine.Clear();
            _followMouseSaveTimer = 0;
        }

        if (Input.GetMouseButton(0))
        {
            _timer += Time.deltaTime;
        }

        if (_isClicked)
        {
            var mousePositionCurrent = Input.mousePosition;
            mousePositionCurrent.z = 0;
            if (_followMouseSaveTimer <= 0 && _camera != null)
            {
                var worldClickPosition = _camera.ScreenToWorldPoint(mousePositionCurrent);
                worldClickPosition.z = 0;
                _mousePositionsLine.Add(worldClickPosition);
            }

            _followMouseSaveTimer += Time.deltaTime; // For a small optimization so I add not All of the positions from Update but from a curtain range
            if (_followMouseSaveTimer >= 0.1f)
            {
                _followMouseSaveTimer = 0;
            }
        }

        if (Input.GetMouseButtonUp(0) && _isClicked)
        {
            var mousePositionCurrent = Input.mousePosition;
            if (_camera != null)
            {
                _playerTargetPos = _camera.ScreenToWorldPoint(mousePositionCurrent);
                _playerTargetPos.z = 0;
            }

            if (_timer > 0.17f) // to check if it it a click or a follow line
            {
                LineFollowed?.Invoke(_mousePositionsLine);
            }
            else
            {
                MoveStarted?.Invoke(_playerTargetPos, true);
            }
            
            _timer = 0;
            _isClicked = false;
        }
    }
}