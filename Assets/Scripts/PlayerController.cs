using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   private ITapListener _tapListener;
   private float _playerSpeed = 0;
   private Vector3 _targetPosition = Vector3.zero;
   private Vector3 _playerCachedPos = Vector3.zero;
   private bool _isMoving;
   private bool _isFollowing;
   private int _currentPosIndex = 0;
   private List<Vector3> _positionsToFollow = new List<Vector3>();

   private void OnEnable()
   {
      _tapListener = gameObject.AddComponent<TapListener>();
      _tapListener.MoveStarted += OnMoveStarted;
      _tapListener.LineFollowed += OnLineFollowed;
   }
   
   private void OnDisable()
   {
      _tapListener.MoveStarted -= OnMoveStarted;
      _tapListener.LineFollowed -= OnLineFollowed;
   }
   
   private void FixedUpdate()
   {
      if (_isMoving)
      {
         MoveToClickPos();
      }

      if (_isFollowing)
      {
         FollowMouse();
      }
   }

   private void OnLineFollowed(List<Vector3> positions)
   {
      _positionsToFollow = positions; // To follow the cursor
      _isFollowing = true;
   }

   private void OnMoveStarted(Vector3 targetPosition, bool isMoving)
   {
      _playerSpeed = 0;
      _targetPosition = targetPosition;
      _isMoving = isMoving;
   }

   private void MoveToClickPos()
   {
      if ((transform.position - _targetPosition).sqrMagnitude > 0.001f)
      {
         var position = transform.position;
         var distance = Vector3.Distance(position, _targetPosition);
         position = Vector3.Lerp(position, _targetPosition, _playerSpeed);
         transform.position = position;
         _playerSpeed += Time.deltaTime * 0.5f / distance; // slowing down at the end. If different curves are needed DOtween can help with its Ease.Something
      }
      else
      {
         transform.position = _targetPosition;
         DataStorage.Distance = Vector3.Distance(_playerCachedPos, _targetPosition);
         _playerCachedPos = _targetPosition;
         _isMoving = false;
         _playerSpeed = 0;
      }
   }

   private void FollowMouse()
   {
      if (_currentPosIndex >= _positionsToFollow.Count)
      {
         _isFollowing = false;
         _currentPosIndex = 0;
         return;
      }

      var targetPosition = _positionsToFollow[_currentPosIndex];
      float distance = Vector3.Distance(transform.position, targetPosition);

      if (distance > 0.1f)
      {
         transform.position = Vector3.MoveTowards(transform.position, targetPosition, 6f * Time.deltaTime);
      }
      else
      {
         _currentPosIndex++;
      }
   }
   
   private void OnMouseDown()
   {
      _targetPosition = transform.position;
      _isMoving = false;
   }
}