using System;

public static class DataStorage 
{
  private static int _score;
  public static event Action ScoreChanged;
  private static float _distance;
  public static event Action DistanceChanged;
  public static int Score
  {
    get => _score;
    set
    {
      if (_score == value)
      {
        return;
      }
      _score = value;
      ScoreChanged?.Invoke();
    }
  }

  public static float Distance
  {
    get => _distance;
    set
    {
      if (Math.Abs(_distance - value) < 0.001f)
      {
        return;
      }
      _distance = value;
      DistanceChanged?.Invoke();
    }
  }
}